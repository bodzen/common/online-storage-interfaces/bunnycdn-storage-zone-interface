#!/usr/bin/python3
# -*- coding: utf-8 -*-


__all__ = ['StorageZoneInterface_not_implemented_method',
           'StorageZoneInterface_InvalidFileToUpload']


class CustomException(Exception):
    def __init__(self, *args) -> None:
        error_msg = self.build_error_msg(*args)
        super().__init__(error_msg)


class StorageZoneInterface_not_implemented_method(CustomException):
    @staticmethod
    def build_error_msg(method_name: str):
        err_msg = 'Method "{0}" is not implemented in BunnyCDN ' \
                  'Storage Zone interface.'.format(method_name)
        return err_msg


class StorageZoneInterface_InvalidFileToUpload(CustomException):
    @staticmethod
    def build_error_msg(filepath: str):
        err_msg = 'Cannot find the file "{0}" ' \
                  'to upload'.format(filepath)
        return err_msg
