#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This module provides an interface for BunnyCDN
    Storage Zones
"""


from bunnyCDN_storage_zone_interface.interface import \
    szone_interface

from bunnyCDN_storage_zone_interface.szone_exceptions import \
    StorageZoneInterface_not_implemented_method
