#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import requests
from requests.utils import quote
import ntpath
from bunnyCDN_storage_zone_interface.szone_exceptions import \
    StorageZoneInterface_not_implemented_method, \
    StorageZoneInterface_InvalidFileToUpload


__all__ = ['szone_interface']


class szone_interface:
    """
        Interface to BonnyCDN's storage zones
        It can handle the following method:
            - put ~> to upload a file
            - delete ~> to delete a file
    """
    def __init__(self, zone_name: str, path: str):
        """
            - param0: (str) name of bunnyCDN storage zone
            - param1: (str) path where the given file(s) will be upload
                            in the given storage zone

        """
        self.base_url = 'https://storage.bunnycdn.com/{0}/{1}/'.format(zone_name,
                                                                       path)
        self.headers = {
            'AccessKey': self.__get_access_key_from_secret(),
            'Checksum': ''
        }

    @staticmethod
    def __get_access_key_from_secret():
        return os.environ['BUNNYCDN_ACCESS_KEY']

    def upload(self, filepath):
        """
            Method to upload a given file to the storage zone
                - param0: (str) filepath to upload
        """
        return self.launch_request(filepath, 'put')

    def delete(self, filename):
        """
            Method to delete a given file from the storage zone
                - param0: (str) filename to delete
        """
        return self.launch_request(filename, 'delete')

    def download(self, filename):
        """
            Method to down a given file from the storage zone.
            The file will be created in the active directory.
                - param0: (str) filename to download
        """
        response = self.launch_request(filename, 'get')
        self.__write_file(filename, response)
        return response

    @staticmethod
    def __write_file(filename: str, response: requests.models.Response):
        content = response.content
        with open(filename, 'wb') as f:
            f.write(content)

    def launch_request(self, filepath: str, given_method: str):
        """
            Generic request to handle the exchange with distant server
        """
        dest_url = self.__build_dest_url(filepath)
        request_with_method = self.__get_request_method(given_method)
        content_fd = self.__get_content_fd(given_method, filepath)
        response = request_with_method(dest_url,
                                       headers=self.headers,
                                       data=content_fd)
        if content_fd:
            content_fd.close()
        return self.__handle_response(response)

    def __build_dest_url(self, filepath: str) -> str:
        filename = ntpath.basename(filepath)
        dest_url = self.base_url + quote(filename, safe='')
        return dest_url

    @staticmethod
    def __get_content_fd(given_method: str, filepath: str):
        if given_method == 'put':
            try:
                return open(filepath, 'rb')
            except FileNotFoundError:
                raise StorageZoneInterface_InvalidFileToUpload(filepath)
        return None

    @staticmethod
    def __get_request_method(given_method):
        try:
            return getattr(requests, given_method)
        except AttributeError:
            raise StorageZoneInterface_not_implemented_method(given_method)

    def __handle_response(self, response: requests.models.Response):
        """
            WIP
        """
        expected_success_code = self.__get_expected_success_code(response)
        if (response.status_code != expected_success_code):
            raise Exception('Something went wrong!')
        return response

    @staticmethod
    def __get_expected_success_code(response: requests.models.Response) -> int:
        method = response.request.method
        if method == 'PUT':
            return 201
        elif method in ('DELETE', 'GET'):
            return 200
        raise StorageZoneInterface_not_implemented_method(method)
