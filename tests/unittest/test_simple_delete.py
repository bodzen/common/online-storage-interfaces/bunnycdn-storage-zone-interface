#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from string import hexdigits
import unittest
from bunnyCDN_storage_zone_interface import \
    szone_interface
from os import remove
from parameterized import parameterized  # pyre-ignore


class simple_delete(unittest.TestCase):
    def setUp(self):
        szone_name = 'ci-zone'
        self.inst = szone_interface(szone_name, 'python_interface_tests')

    @parameterized.expand([
        ['alpha', '1024'],
        ['beta', '2048'],
        ['gamma', '4096'],
        ['delta', '8192']
    ])
    def test_generic(self, name: str, filesize: str):
        filename = self._create_remote_file(filesize)

        response = self.inst.upload(filename)
        self.assertEqual(response.status_code, 201)

        response = self.inst.delete(filename)
        self.assertEqual(response.status_code, 200)

        self.assertRaises(Exception,
                          self.inst.download, filename)
        remove(filename)

    def _create_remote_file(self, filesize: str):
        random_str = ''.join(random.choices(hexdigits, k=10))
        filename = 'simple_upload_test_' + filesize + random_str + '.remote'
        with open(filename, 'w') as f:
            f.truncate(int(filesize))
        return filename
