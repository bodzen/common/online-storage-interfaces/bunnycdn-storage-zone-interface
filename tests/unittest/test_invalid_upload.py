#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from bunnyCDN_storage_zone_interface import \
    szone_interface
from bunnyCDN_storage_zone_interface.szone_exceptions import \
    StorageZoneInterface_InvalidFileToUpload
from parameterized import parameterized  # pyre-ignore


class invalid_upload(unittest.TestCase):
    def setUp(self):
        szone_name = 'ci-zone'
        self.inst = szone_interface(szone_name, 'python_interface_tests')

    @parameterized.expand([
        ['alpha', '/tmp/invalid00'],
        ['beta', 'invalid01'],
    ])
    def test_generic(self, name: str, filename: str):
        self.assertRaises(StorageZoneInterface_InvalidFileToUpload,
                          self.inst.upload, filename)
