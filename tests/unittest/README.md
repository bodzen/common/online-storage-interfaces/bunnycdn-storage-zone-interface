# How To

1. Simulate than a secret was mount:
```
sudo mkdir -p /etc/bunnycdn-credentials/
sudo chown `whoami`: /etc/bunnycdn-credentials/
echo '15273e3c-a518-41e4-841de5ac0cce-e426-42b8' > /etc/bunnycdn-credentials/accesskey
```

2. Launch the tests:
```
cd /PATH/TO/bunnyCDN_storage_zone_interface
python -m unittest discover tests/unittest/
```
