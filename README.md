# BunnyCDN's Storage Zones Interface

Interface to BunnyCDN's storage zones.

Currently it can:
* upload
* delete
* download

## How to

The credentials (=AccessKeys) must be fetch from a mounted secrets in the bellow path
```
/etc/bunnycdn-credentials/accesskey
```

From any python projects:
```
from bunnyCDN_storage_zone_interface import \
	szone_interface

inst = szone_interface('STORAGE_ZONE_NAME', 'PATH_IN_SZONE')
inst.upload('my_file.txt')
inst.download('my_file.txt')
inst.delete('my_file.txt')
```
