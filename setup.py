#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup  # pyre-ignore
from pip._internal.req import parse_requirements


def load_requirements():
    required_lib_list = parse_requirements('requirements.txt', session="test")
    return [str(lib.requirement) for lib in required_lib_list]

setup(
    name='bunnyCDN_storage_zone_interface',
    version='omega-3',
    author='dh4rm4',
    description='Provides an interface for BunnyCDN Storage Zones',
    install_requires=load_requirements(),
    classifiers=[
        "Programming Language :: Python 3.8.3",
        "Development Status :: Never ending",
        "Language :: English",
        "Operating System :: Debian based",
    ],
)
